from flask import Flask, request, render_template
import pickle
import numpy as np
import pandas as pd

app = Flask(__name__)

# Load the model
try:
    model = pickle.load(open('pipe.pkl', 'rb'))
except Exception as e:
    print(f"Error loading model: {e}")
    model = None

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict', methods=['POST'])
def predict():
    if model is None:
        return render_template('index.html', prediction_text='Model not available.')
    
    if request.method == 'POST':
        try:
            company = request.form['company']
            type_name = request.form['type_name']
            ram = int(request.form['ram'])
            weight = float(request.form['weight'])
            touchscreen = 1 if request.form['touchscreen'] == 'Yes' else 0
            ips = 1 if request.form['ips'] == 'Yes' else 0
            ppi = float(request.form['ppi'])
            cpu_brand = request.form['cpu_brand']
            hdd = int(request.form['hdd'])
            ssd = int(request.form['ssd'])
            gpu_brand = request.form['gpu_brand']
            os = request.form['os']
            
            # Create the input array for the model
            input_data = pd.DataFrame([[company, type_name, ram, weight, touchscreen, ips, ppi, cpu_brand, hdd, ssd, gpu_brand, os]], 
                                    columns=['Company', 'TypeName', 'Ram', 'Weight', 'Touchscreen', 'Ips', 'ppi', 'Cpu brand', 'HDD', 'SSD', 'Gpu brand', 'os'])
            
            # Predict the price
            prediction = model.predict(input_data)[0]
            predicted_price = np.exp(prediction)  # Remember, the target variable was log-transformed
            
            return render_template('result.html', prediction_text=f'Predicted Laptop Price: Nu.{predicted_price:.2f}')
        
        except Exception as e:
            return render_template('index.html', prediction_text=f'Error in prediction: {e}')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
